import { Routes, Route } from "react-router-dom";
import { pageRoutes } from "constants/pageRoutes";
import { HomePage, GaragePage } from "pages";

function AppRouter() {
  return (
    <Routes>
      <Route path={pageRoutes.HOME} element={<HomePage />} />
      <Route path={pageRoutes.GARAGE} element={<GaragePage />} />
    </Routes>
  );
}

export default AppRouter;
